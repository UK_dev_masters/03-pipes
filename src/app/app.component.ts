import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  nombre = "Argoitz"
  nombre2 = "arGoItz estebAnez baRRado"
  arr = [1,2,3,4,5,6,7,8,9,10];
  PI = Math.PI;
  a:number = 0.234;
  salario = 1234.5;

  heroe = {
    nombre: "Logan",
    clave: "Lovezno",
    edad: 500,
    direccion: {
      calle: "Primera",
      casa: "5"
    }
  }

  valorDePromera = new Promise( (resolve, reject)=> {
    setTimeout(() => resolve('llego la data!'), 3500);
  });

  fecha = new Date();
  video = "K5jvUXij7nU";
  activar = true;
}
