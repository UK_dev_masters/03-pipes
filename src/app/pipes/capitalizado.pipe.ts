import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizado'
})
export class CapitalizadoPipe implements PipeTransform {

  // transform(value: string, ...args: any[]): string {   --> All args
  // transform(value: string, arg1,arg2,arg3): string { --> All as array
  transform(value: string, todas:boolean = true ): string {

    value = value.toLowerCase();
    let nom = value.split(" ");
    if(todas){
      for(let i in nom ){
        nom[i] = nom[i][0].toUpperCase() + nom[i].substring(1);

      }
    }else{
      nom[0] = nom[0][0].toUpperCase() + nom[1].substring(1); 
    }

    return nom.join(" ");
  }

}
