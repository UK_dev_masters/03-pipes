import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contrasena'
})
export class ContrasenaPipe implements PipeTransform {

  transform(value: any, ocultar: boolean = true): any {
    value = value.split("");
    if(ocultar){
      for(let i in value){
        value[i] = "*";
      }
    }
    return value.join("");
  }

}
